#!/bin/bash 
# Authors: S. Schwalbe 
# Date:    21.03.2024 

# Status: 
#	Check it for python3.7,python3.10 and python3.11 

# Why this script? 
# 	 -> issue 3 
#	 -> https://numpy.org/devdocs/reference/distutils_status_migration.html#distutils-status-migration
#	f2py does not work properly with numpy.distutils and setuptools anymore 

# ERRORS? 
# We need dev header 
# 	If you get an error like "python.h" missing, try something like 
# 	Ubuntu/Debian 
# 	sudo apt-get install python3-dev

# Variables 
# python version 
# You can this to, e.g, python3.10 or python3.11 
py=python3 
MYHOME=$(pwd)
install_packages=true 
build_fortran=true 
build_python=true 
run_check=false

# Install python packages 
if $install_packages;
then
	echo "Install: Python packages"
	$py -m pip install numpy --user &> pip.log  # for f2py 
	$py -m pip install ase --user &> pip.log    # for HEA 
	$py -m pip install tk --user &> pip.log     # for GUI 
	$py -m pip list | grep 'numpy' 
	$py -m pip list | grep 'ase' 
	$py -m pip list | grep 'tk' 
fi

# Install: Fortran part of porE 
if $build_fortran;
then
	cd porE/lib 
	echo "Build: Fortran routines" 
	# build 
	# numpy.f2py.f2py2e is an alternative for the 1st step 
	$py -m numpy.f2py porE.f90  -m pore  -h pore.pyf &> f2py_1.log 
	# the compilation setp can only be done outside of python :/ 
	$py -m numpy.f2py -c pore.pyf porE.f90	&> f2py_2.log 
	$py -c "from pore import porosity"
	echo "pore shared library can be loaded."

	# copy so 
	cp *.so $MYHOME/
	
	# clean  
	rm pore.pyf
	rm pore.cpython-*.so
	rm *.log 
	cd $MYHOME
fi 

# Install: Python part of porE
if $build_python;
then
	echo "Build: Python routines"
	$py -m pip install -e . &> py.log 
	rm *.log 
fi 

# Run: Check 
if $run_check;
then
	echo "Run: example"
	cd examples 
	$py run_porE.py &> test_porE.log  
	tail -n1 test_porE.log 
	rm test_porE.log 	
	cd $MYHOME
fi 
echo "porE is hopefully installed. ;)"
