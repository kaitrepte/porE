# Main developer      
Core routines, code structure and working examples.

#### Kai Trepte, PhD   
email: kai.trepte1987@gmail.com
- all main FORTRAN routines

#### Sebastian Schwalbe, PhD         
email: theonov13@gmail.com
- GUI for porE
- source code structuring
- ideas for improvements of porE (simplification of input)
